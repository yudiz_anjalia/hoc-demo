import logo from './logo.svg';
import './App.css';
import File1 from './components/File1';
import File2 from './components/File2';

function App() {
  return (
    <div>
      <br/>
      <br/>
      <h1>High-Order Component</h1>
      <br/>
      <File1 />
      <br/>
      <br/>
      <File2 />
    </div>
  );
}

export default App;
