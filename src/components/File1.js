import React, { useState } from 'react'
import Counter from './HOC'

function File1(props) {
    const{count , increment , name} = props
  return (

    <div className='file1'>
        <button className='btn' onClick={increment}> {name} {count} Click</button>
    </div>
  )
}

export default Counter(File1 , 2);