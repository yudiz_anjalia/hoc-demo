import React , {useState} from 'react'
import Counter from './HOC'

function File2(props) {
    const{count , increment , name } = props
  return (

    <div className='file2'>
        <button onMouseOver={increment}> {name} {count} MouseOver</button>
    </div>
  )
}
export default Counter(File2 ,3 );